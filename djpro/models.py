from django.contrib.auth.models import User
from django.db import models


class Partner(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)


class Event(models.Model):
    partner = models.ForeignKey(Partner, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    time_from = models.DateTimeField()
    time_to = models.DateTimeField()
