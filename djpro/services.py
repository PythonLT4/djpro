from djpro.models import Event
from djpro.models import Partner


def get_partner_events(partner: Partner):
    return Event.objects.filter(partner=partner)