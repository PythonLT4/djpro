from django.contrib.auth.decorators import login_required
from django.http import HttpRequest
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import render

from djpro.models import Partner
from djpro.services import get_partner_events


@login_required
def partner_calendar(request: HttpRequest) -> HttpResponse:
    partner = get_object_or_404(Partner, user=request.user)
    return render(request, 'calendar.html', {
        'calendar': get_partner_events(partner),
    })
