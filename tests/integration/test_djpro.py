import datetime

import pytest
from django.contrib.auth.models import User
from django.test import Client

from djpro.models import Event
from djpro.models import Partner


def test_index_anonymous(client: Client):
    resp = client.get('/')
    assert resp.status_code == 302
    assert resp.headers['location'] == '/accounts/login/?next=/'


@pytest.mark.django_db
def test_partner_calendar_empty(client: Client):
    user = User.objects.create_user(
        username='partner',
        email='parner@example.com',
        password='secret',
    )
    Partner.objects.create(
        user=user,
    )

    client.login(username=user.username, password='secret')

    resp = client.get('/')

    assert resp.status_code == 200
    assert list(resp.context['calendar']) == []


@pytest.mark.django_db
def test_partner_calendar(client: Client):
    user = User.objects.create_user(
        username='partner',
        email='parner@example.com',
        password='secret',
    )
    partner = Partner.objects.create(
        user=user,
    )
    now = datetime.datetime.utcnow()
    day = datetime.timedelta(days=1)
    event = Event.objects.create(
        partner=partner,
        name='Event 1',
        time_from=now + day,
        time_to=now + 2 * day,
    )

    client.login(username=user.username, password='secret')

    resp = client.get('/')

    assert resp.status_code == 200
    assert list(resp.context['calendar']) == [event]
